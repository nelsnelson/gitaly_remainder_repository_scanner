# frozen_string_literal: true

# vi: set ft=ruby :

# -*- mode: ruby -*-

require 'gitaly_management/config'

# GitalyManagement module
module GitalyManagement
  # Logging module
  module Logging
    TIMESTAMP_FORMAT = ::GitalyManagement::Config::DEFAULTS[:log_timestamp_format]

    def initialize_log
      STDOUT.sync = true
      log = Logger.new($stdout)
      log.level = Logger::INFO
      log.formatter = proc do |level, t, _name, msg|
        fields = { timestamp: t.strftime(TIMESTAMP_FORMAT), level: level, msg: msg }
        Kernel.format("%<timestamp>s %-5<level>s %<msg>s\n", **fields)
      end
      log
    end

    def initialize_progress_log
      log = initialize_log
      log.formatter = proc do |level, t, _name, msg|
        fields = { timestamp: t.strftime(TIMESTAMP_FORMAT), level: level, msg: msg }
        Kernel.format("\r%<timestamp>s %-5<level>s %<msg>s", **fields)
      end
      log
    end

    def log
      @log ||= initialize_log
    end

    def progress_log
      @progress_log ||= initialize_progress_log
    end

    def dry_run_notice
      log.info '[Dry-run] This is only a dry-run -- write operations will be logged but not executed'
    end

    def debug_command(cmd)
      log.debug "Command: #{cmd}"
      cmd
    end
  end
  # module Logging
end
# module GitalyManagement
